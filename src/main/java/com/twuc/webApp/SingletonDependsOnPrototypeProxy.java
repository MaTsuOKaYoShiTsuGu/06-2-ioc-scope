package com.twuc.webApp;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependsOnPrototypeProxy {
    private PrototypeDependentWithProxy prototypeDependentWithProxy;

    public PrototypeDependentWithProxy getPrototypeDependentWithProxy() {
        return prototypeDependentWithProxy;
    }

    public SingletonDependsOnPrototypeProxy(PrototypeDependentWithProxy prototypeDependentWithProxy) {
        this.prototypeDependentWithProxy = prototypeDependentWithProxy;
    }

    public void callPrototypeMethod(){
        prototypeDependentWithProxy.someMethod();
    }
}
