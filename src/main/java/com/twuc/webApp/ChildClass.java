package com.twuc.webApp;

import org.springframework.stereotype.Component;

@Component
public class ChildClass extends FatherClass{

    public ChildClass() {
        log.add("child");
    }

}
