package com.twuc.webApp;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependsOnPrototypeProxyBatchCall {
    private PrototypeDependentWithProxy prototypeDependentWithProxy;

    public PrototypeDependentWithProxy getPrototypeDependentWithProxy() {
        return prototypeDependentWithProxy;
    }

    public SingletonDependsOnPrototypeProxyBatchCall(PrototypeDependentWithProxy prototypeDependentWithProxy) {
        this.prototypeDependentWithProxy = prototypeDependentWithProxy;
        Log.add("singleton");
    }

}
