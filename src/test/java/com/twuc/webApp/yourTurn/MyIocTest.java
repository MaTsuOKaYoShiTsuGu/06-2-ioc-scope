package com.twuc.webApp.yourTurn;

import com.twuc.webApp.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.util.ClassUtils;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class MyIocTest {

    private AnnotationConfigApplicationContext context;

    @BeforeEach
    void setContext(){
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        Log.add("container");
    }

    @Test
    void should_be_the_same_object_between_interface_and_implements() {
        InterfaceOneImpl  interfaceOneImpl = context.getBean(InterfaceOneImpl.class);
        InterfaceOne interfaceOne = context.getBean(InterfaceOne.class);
        assertSame(interfaceOneImpl,interfaceOne);
    }

    @Test
    void should_be_the_same_object_between_extends() {
        ChildClass childClass = context.getBean(ChildClass.class);
        FatherClass fatherClass = context.getBean(FatherClass.class);
        assertSame(childClass,fatherClass);
    }

    @Test
    void should_only_have_child_message() {
        FatherClass fatherClass = context.getBean(FatherClass.class);
        assertEquals(ChildClass.class,fatherClass.getClass());
    }

    @Test
    void should_be_the_same_object_between_abstract_extends() {
        AbstractBaseClass abstractBaseClass = context.getBean(AbstractBaseClass.class);
        DerivedClass derivedClass = context.getBean(DerivedClass.class);
        assertSame(abstractBaseClass,derivedClass);
    }

    @Test
    void should_be_two_object_when_create_multiple_prototype_object() {
        SimplePrototypeScopeClass prototype1 = context.getBean(SimplePrototypeScopeClass.class);
        SimplePrototypeScopeClass prototype2 = context.getBean(SimplePrototypeScopeClass.class);
        assertNotSame(prototype1,prototype2);
    }

    @Test
    void should_get_singleton_prototype_container_create_order() {
        SingletonDependsOnPrototypeProxyBatchCall singleton = context.getBean(SingletonDependsOnPrototypeProxyBatchCall.class);
        singleton.getPrototypeDependentWithProxy().someMethod();
        ArrayList<String> actual = Log.getLog();
        assertEquals(3,actual.size());
        assertEquals("singleton",actual.get(0));
        assertEquals("container",actual.get(1));
        assertEquals("prototype",actual.get(2));
    }

    @Test
    void should_get_double_object_using_double_context() {
        AnnotationConfigApplicationContext context2 =
                new AnnotationConfigApplicationContext("com.twuc.webApp");
        SimpleSingletonClass singleton1 = context.getBean(SimpleSingletonClass.class);
        SimpleSingletonClass singleton2 = context2.getBean(SimpleSingletonClass.class);
        assertNotSame(singleton1,singleton2);
    }

    @Test
    void should_get_2_prototype_objects_and_1_singleton_object_when_prototype_depend_on_singleton() {
        PrototypeScopeDependsOnSingleton prototype1 = context.getBean(PrototypeScopeDependsOnSingleton.class);
        PrototypeScopeDependsOnSingleton prototype2 = context.getBean(PrototypeScopeDependsOnSingleton.class);
        assertNotSame(prototype1,prototype2);
        assertSame(prototype1.getSingletonDependent(),prototype2.getSingletonDependent());
    }

    @Test
    void should_get_1_prototype_object_and_1_singleton_object_when_singleton_depend_on_prototype() {
        SingletonDependsOnPrototype singleton1 = context.getBean(SingletonDependsOnPrototype.class);
        SingletonDependsOnPrototype singleton2 = context.getBean(SingletonDependsOnPrototype.class);
        assertSame(singleton1.getPrototypeDependent(),singleton2.getPrototypeDependent());
    }

    @Test
    void should_create_2_different_prototype_objects_via_proxy() {
        SingletonDependsOnPrototypeProxy singleton1 = context.getBean(SingletonDependsOnPrototypeProxy.class);
        int count1 = singleton1.getPrototypeDependentWithProxy().someMethod();
        SingletonDependsOnPrototypeProxy singleton2 = context.getBean(SingletonDependsOnPrototypeProxy.class);
        int count2 = singleton2.getPrototypeDependentWithProxy().someMethod();
        assertEquals(count1,count2);
    }

    @Test
    void should_create_2_prototype_object_calling_1_singleton_object_twice() {
        SingletonDependsOnPrototypeProxyBatchCall singleton = context.getBean(SingletonDependsOnPrototypeProxyBatchCall.class);
        String string1 = singleton.getPrototypeDependentWithProxy().toString();
        String string2 = singleton.getPrototypeDependentWithProxy().toString();
        assertNotEquals(string1,string2);
    }

    @Test
    void should_be_a_proxy_class() {
        SingletonDependsOnPrototypeProxy singleton = context.getBean(SingletonDependsOnPrototypeProxy.class);
        assertTrue(ClassUtils.isCglibProxyClass(singleton.getPrototypeDependentWithProxy().getClass()));
    }
}
